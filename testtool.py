import requests
from requests.structures import CaseInsensitiveDict
import click
import os.path


@click.command()
@click.argument('token')
@click.argument('method')
@click.argument('src_path')
@click.argument('dst_path')
def main(token, method, src_path, dst_path):
    check_url = "https://api.dropboxapi.com/2/check/user"
    headers = CaseInsensitiveDict()
    headers["Authorization"] = f"Bearer {token}"
    headers["Content-Type"] = "application/json"
    data = '{"query":""}'
    try:
        resp = requests.post(check_url, headers=headers, data=data)
        if resp.status_code == 200:
            if method == 'get':
                if not os.path.exists((os.path.split(dst_path)[0])) and '/' in dst_path:
                    print('Invalid dst_path')
                else:
                    get_url = "https://content.dropboxapi.com/2/files/download"
                    headers = CaseInsensitiveDict()
                    headers["Authorization"] = f"Bearer {token}"
                    headers["Dropbox-API-Arg"] = f"{{\"path\": \"{src_path}\"}}"
                    resp = requests.post(get_url, headers=headers)
                    if resp.status_code == 200:
                        with open(f"{dst_path}", 'wb') as f:
                            f.write(resp.content)
                        print('Success!')
                    else:
                        print(resp.content.decode("utf-8"))
            elif method == 'put':
                put_url = "https://content.dropboxapi.com/2/files/upload"
                if os.path.exists(src_path):
                    headers = CaseInsensitiveDict()
                    headers["Authorization"] = f"Bearer {token}"
                    headers["Dropbox-API-Arg"] = f"{{\"path\": \"{dst_path}\",\"mode\": \"add\",\"autorename\": true," \
                                                 f"\"mute\": false,\"strict_conflict\": false}}"
                    headers["Content-Type"] = "application/octet-stream"
                    with open(f'{src_path}', 'rb') as data:
                        resp = requests.post(put_url, headers=headers, data=data)
                    if resp.status_code == 200:
                        print('Success!')
                    else:
                        print(resp.content.decode("utf-8"))
                else:
                    print('Invalid src_path')
            else:
                print('Invalid method')
        else:
            print(resp.content.decode("utf-8"))
    except (requests.ConnectionError, requests.Timeout):
        print('The requested url is not available')


if __name__ == '__main__':
    main()
