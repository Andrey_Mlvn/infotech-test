# Тестовое задание

Консольная утилита позволяет загружать в/скачивать из Dropbox произвольные файлы с помощью формирования HTTP-запросов к REST API, т.е. не используя оберточных библиотек (например, Dropbox SDK).


## Оглавление

  - [Описание](#описание)
  - [Установка](#установка)
  - [Получение Access token для использования утилиты](#получение-access-token-для-использования-утилиты)
  - [Инструкция](#инструкция)

## Описание

Консольная утилита осуществляет взаимодействие с Dropbox через передаваемые параметры: token, method, src_path, dst_path.

### Установка

Для использования утилиты необходимо установить библиотеки командой:

    pip install -r requirements.txt

## Получение access token для использования утилиты

Для использования утилиты пользователю необходимо получить access token.<br>
APP_KEY для данного приложения: hkwsgtq6qlf642w<br>
APP_SECRET для данного приложения: 21lb07c16das3d4

1) По URL https://www.dropbox.com/oauth2/authorize?client_id=<APP_KEY>&response_type=code пользователю необходимо авторизироваться в Dropbox.<br>
После этого пользователь получит Authorization code.

2) Имея Authorization code, необходимо получить Access token для дальнейшего взаимодействия с API.

В ответе на запрос к https://api.dropboxapi.com/oauth2/token будет содержаться access_token.<br>
Пример запроса на python:

    import requests
    
    url = "https://api.dropbox.com/oauth2/token"
    access_token = 'W7rvFPwlb8YAAAAAAAAADnfNZyRLcdz9AeF7Rd3LoSw'    
    payload = f'grant_type=authorization_code&code={access_token}'
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Cookie': 'locale=en; t=bsTeGf4P2FgqhIYwGI3TTZeK'
    }

    response = requests.request("POST", url, headers=headers, data=payload, auth=('hkwsgtq6qlf642w', '21lb07c16das3d4'))
    response.json()["access_token"]

## Инструкция

Вызов утилиты выглядит так:

python testtool.py access_token put/get src_path dst_path

После вызова put соответствующий файл src_path загружается на сервер по пути dst_path.
После вызова get файл, находящийся на сервере под именем src_path скачивается и сохраняется по локальному пути dst_path.

Авторизация реализована через сгенерированный access token.

Пример использования для загрузки файла на сервер:

    python testtool.py z3TKus8SWOsAAAAAAAAAARjOhD4huA7yAK2eOlZ0Fj9e3JyqUQ2Pa654wxBMpq1t put 1.pdf /folder/5.pdf

Пример использования для загрузки файла c сервера:

    python testtool.py z3TKus8SWOsAAAAAAAAAARjOhD4huA7yAK2eOlZ0Fj9e3JyqUQ2Pa654wxBMpq1t get /1.pdf 5.pdf

Успешное выполнение:

    Success!

Примеры ошибок при выполнении:

    Invalid method
    Invalid src_path
    Error in call to API function "files/upload": HTTP header "Dropbox-API-Arg": path: '8.pdf' did not match pattern '(/(.|[\r\n])*)|(ns:[0-9]+(/.*)?)|(id:.*)'
